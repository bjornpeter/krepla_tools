﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using ISD.CAD.Contexts;
using ISD.CAD.Views;
using ISD.CAD.Data;
using ISD.CAD.IO;
using ISD.CAD.Interface;
using ISD.CAD.Steel;
using ISD.GUI.Library.Collections.Touples;
using ISD.GUI.Library.Controller;
using ISD.GUI.Library.View;
using ISD.Math;
using Smart_Printer.Controller;
using ISD.IFC2X3;

namespace Smart_Printer.Windows
{
    /// <summary>
    /// Interaction logic for IWSP.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged, IViewWindow
    {
        #region Interface: INotifyPropertyChanged

        public static Dictionary<int, Figure> dicBorders;
        public List<Figure> borders = new List<Figure>();
        public DrawingSheet ds = Ucontext.ActiveScene.ActiveDrawingSheet;
        public ObjectSettings os = new ObjectSettings();
        public List<FileInfo> pdfFile = new List<FileInfo>();
        private Spooler Printer = new Spooler("PDFCreator");

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        #endregion Interface: INotifyPropertyChanged

        #region Interface: IViewWindow

        private IViewWindowController viewController;
        private IControllerBase viewControllerBase;
        private IntPtr viewParent;

        public void AddWindowController(IViewWindowController windowcontrollerinterface) => this.viewController = windowcontrollerinterface;

        public IntPtr GetWindowHandle() => this.GetWindowHandle();

        public void Init(IControllerBase controllerinterface) => this.viewControllerBase = controllerinterface;

        public void SetParent(IntPtr parent) => this.viewParent = parent;

        #endregion Interface: IViewWindow

        public MainWindow()
        {
            InitializeComponent();
            grdSP.DataContext = os;
            os.ObjectProjectNummer = Ucontext.ActiveScene.AttributeSet.GetValue<string>("_SZNATTRS01");
            os.ObjectLocatie = Ucontext.ActiveScene.Name;
            if (os.ObjectLocatie.Contains("08 TEKENING"))
            {
                int indexnummer = os.ObjectLocatie.LastIndexOf("08 TEKENING");
                os.ObjectLocatie = os.ObjectLocatie.Substring(0, indexnummer + 12);
            }
            else
            {
                int indexnummer = os.ObjectLocatie.LastIndexOf("\\");
                os.ObjectLocatie = os.ObjectLocatie.Substring(0, indexnummer + 1);
            }
            os.ObjectLocatie.ToLower();
            // tbLocation.Text = Ucontext.ActiveScene.ActiveNode.Properties.;
        }

        private static UnconstrainedContext Ucontext => ISD.Scripting.ScriptBase.BaseContext as UnconstrainedContext;

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = os.ObjectLocatie;
            if (System.Windows.Forms.DialogResult.OK == fbd.ShowDialog())
            {
                os.ObjectLocatie = fbd.SelectedPath + "\\";
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("no folder selected");
            };
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            if (!os.DwgperSheet && !os.PdfperBorder && !os.PdfperSheet)
            {
                System.Windows.Forms.MessageBox.Show("No Export Format Selected Selected");
                return;
            }
            if (string.IsNullOrEmpty(os.ObjectProjectNummer))
            {
                System.Windows.Forms.MessageBox.Show("No valid Filename entered");
                return;
            }

            if (string.IsNullOrEmpty(os.ObjectLocatie))
            {
                System.Windows.Forms.MessageBox.Show("No valid location entered");
                return;
            }

            this.Hide();
            try
            {
                ds.Activate();
                //SortBorders();

                if (os.DwgperSheet)
                {
                    ExportToDWG();
                }

                if (os.PdfperBorder)
                {
                    Printer.Spool(ds.Figures.Where(x => x.Name.ToUpper().Contains("DIN")).ToList(), ds, os.ObjectProjectNummer, os.ObjectLocatie);
                    Printer.CreatePdf(false, false);
                }

                if (os.PdfperSheet)
                {
                    Printer.Spool(ds.Figures.Where(x => x.Name.ToUpper().Contains("DIN")).ToList(), ds, os.ObjectProjectNummer, os.ObjectLocatie);
                    Printer.CreatePdf(true, false);
                }
            }
            catch (Exception)
            {
            }

            this.Show();
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            os.ObjectProjectNummer = Ucontext.ActiveScene.AttributeSet.GetValue<string>("_SZNATTRS01");
            os.ObjectLocatie = Ucontext.ActiveScene.Name;
            if (os.ObjectLocatie.Contains("08 TEKENING"))
            {
                int indexnummer = os.ObjectLocatie.LastIndexOf("08 TEKENING");
                os.ObjectLocatie = os.ObjectLocatie.Substring(0, indexnummer + 12);
            }
            else
            {
                int indexnummer = os.ObjectLocatie.LastIndexOf("\\");
                os.ObjectLocatie = os.ObjectLocatie.Substring(0, indexnummer + 1);
            }
        }

        private void CB_pdfborder_Checked(object sender, RoutedEventArgs e)
        {
            CB_pdfSheet.IsChecked = false;
        }

        private void CB_pdfSheet_Checked(object sender, RoutedEventArgs e)
        {
            CB_pdfBorder.IsChecked = false;
        }

        private void ExportToDWG()
        {
            string FileDWG = os.ObjectLocatie + os.ObjectProjectNummer + ".DWG";
            DXFDWGSettings dwg = new DXFDWGSettings()
            {
                DrawingSheet = ds
            };
            try
            {
                ISD.CAD.IO.FileIO.Save(FileDWG, dwg);
            }
            catch (Exception ex)
            {
            }
        }
    } // end  public partial class IW
} // namespace