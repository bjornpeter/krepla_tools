﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smart_Printer.Controller
{
    public class ObjectSettings : BaseClass
    {
        private bool dwgpersheet;
        private string objectlocatie;
        private string objectprojectnummer;
        private bool pdfperborder;
        private bool pdfpersheet;

        public ObjectSettings()
        {
            pdfpersheet = true;
            dwgpersheet = false;
            pdfperborder = false;
            objectprojectnummer = "";
            objectlocatie = "";
        }

        public bool DwgperSheet
        {
            get { return dwgpersheet; }
            set
            {
                dwgpersheet = value;
                OnPropertyChanged("DwgperSheet");
            }
        }

        public string ObjectLocatie
        {
            get { return objectlocatie; }
            set
            {
                objectlocatie = value;
                OnPropertyChanged("ObjectLocatie");
            }
        }

        public string ObjectProjectNummer
        {
            get { return objectprojectnummer; }
            set
            {
                objectprojectnummer = value;
                OnPropertyChanged("ObjectProjectNummer");
            }
        }

        public bool PdfperBorder
        {
            get { return pdfperborder; }
            set
            {
                pdfperborder = value;
                OnPropertyChanged("PdfperBorder");
            }
        }

        public bool PdfperSheet
        {
            get { return pdfpersheet; }
            set
            {
                pdfpersheet = value;
                OnPropertyChanged("PdfperSheet");
            }
        }
    }
}