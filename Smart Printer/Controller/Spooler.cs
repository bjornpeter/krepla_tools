﻿using System;
using System.Windows;
using ISD.CAD.Data;
using System.Linq;
using Microsoft.Win32;
using System.IO;
using System.Text.RegularExpressions;
using ISD.CAD.IO;
using ISD.CAD.Views;
using System.Collections.Generic;
using System.Diagnostics;
using ISD.CAD.Contexts;

namespace Smart_Printer.Controller
{
    //deze class is 1 op 1 gekopieerd uit de oude smartprinter
    public class Spooler
    {
        public const string GsWin32ArgumentsJoinPDF = "-dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=";
        public const string GsWin32ArgumentsPDF = "-dPDF -dBATCH -dNOPAUSE -dNOOUTERSAVE -dUseCIEColor -sDEVICE=pdfwrite -sOutputFile=";
        public string MergePath = "C:\\temp";
        private List<FileInfo> spooled;

        public Spooler(string PdfPrinter)
        {
            try
            {
                this.PdfPrinterName = PdfPrinterName;
                spooled = new List<FileInfo>();
                //
                HicadPloFile = new DirectoryInfo(Context.Configuration.GetProductPath("0")).Parent.FullName + "\\plo\\HiCAD.PLO";

                // Hicad Printer
                HicadPrinter = new HiCAD_PrintInterface.HiCADPrint();

                // Selecter printer
                string SelectedPrinter = "";
                // Loop througg the printers
                for (int i = 0; i < HicadPrinter.GetPrinterCount(); i++)
                {
                    // Look for required printername
                    if (PdfPrinter == HicadPrinter.GetPrinterName(i))
                    {
                        // Save name and select printer
                        SelectedPrinter = HicadPrinter.GetPrinterName(i);
                        HicadPrinter.SelectPrinter(i);
                        break;
                    }
                }
                // Check if printer was selected
                if (SelectedPrinter == "")
                {
                    MessageBox.Show("Required printer not found: " + PdfPrinter, "Printer Error");
                    HicadPrinter = null;
                }
                else
                {
                    // User color
                    HicadPrinter.bColor = true;
                    // scale line widths
                    HicadPrinter.bScaleLineWidth = false;
                    // Margins
                    HicadPrinter.dPageMarginLeft = 0;
                    HicadPrinter.dPageMarginTop = 0;
                    HicadPrinter.dPageMarginRight = 0;
                    HicadPrinter.dPageMarginBottom = 0;
                    // Scale ( 0=auto)
                    HicadPrinter.dScaling = 0;
                    // Copies
                    HicadPrinter.lCopies = 1;
                    // center drawing
                    HicadPrinter.bAutoCenter = true;
                    // User printable area only
                    HicadPrinter.bPrintableAreaOnly = true;
                    // Qualit ( -1 lowest, -4 best )
                    HicadPrinter.lPrintQuality = -1;
                }

                // Open PDFCreator register settings
                RegistryKey reg = Registry.LocalMachine.OpenSubKey("SOFTWARE\\pdfforge\\PDFCreator\\Program", false);
                Gswin = null;
                Gswin = new DirectoryInfo(reg.GetValue("ApplicationPath").ToString()).GetFiles("gswin*c.exe", SearchOption.AllDirectories).FirstOrDefault();
            }
            catch (Exception ex)
            {
                this.Gswin = null;
                this.HicadPrinter = null;
            }
        }

        public FileInfo Gswin { get; private set; }
        public string HicadPloFile { get; private set; }
        public HiCAD_PrintInterface.HiCADPrint HicadPrinter { get; private set; }
        public string OutputDirectory { get; set; }
        public string PdfPrinterName { get; private set; }

        public bool Valid
        {
            get
            {
                return (HicadPrinter != null && Gswin != null);
            }
        }

        private static UnconstrainedContext Context => (UnconstrainedContext)ISD.Scripting.ScriptBase.BaseContext;

        public void ClearSpooled()
        {
            spooled = new List<FileInfo>();
        }

        public List<FileInfo> CreatePdf(bool merge, bool AllPages)
        {
            List<FileInfo> result = new List<FileInfo>();

            string pdfOut = this.OutputDirectory;

            try
            {
                if (spooled == null || spooled.Count == 0)
                {
                    throw new Exception("Nothing to spool");
                }

                if (merge)
                {
                    if (!Directory.Exists(MergePath))
                    {
                        Directory.CreateDirectory(MergePath);
                    }

                    if (!Directory.Exists(MergePath))
                    {
                        MergePath = Context.Configuration.GetProductPath("#");
                    }

                    string pfdName = Path.GetFileNameWithoutExtension(spooled.First().FullName);
                    pfdName = Regex.Replace(pfdName, "_[0-9]+$", "");
                    if (AllPages)
                    {
                        pfdName = Regex.Replace(pfdName, "_[0-9]+$", "");
                    }

                    List<FileInfo> Delete = new List<FileInfo>();
                    string pdfFile = pdfOut + "\\" + pfdName + ".pdf";
                    result.Add(new FileInfo(pdfFile));

                    string Arguments = GsWin32ArgumentsPDF + "\"" + pdfFile + "\" ";
                    // Copy All PS file to the merge Path
                    foreach (FileInfo s in spooled)
                    {
                        FileInfo ps = s.CopyTo(MergePath + "\\" + s.Name, true);
                        s.Delete();
                        Delete.Add(ps);
                        Arguments += "\"" + ps.FullName + "\" ";
                    }

                    // start new proces
                    Process p = new Process();
                    // process - Executable file
                    p.StartInfo.FileName = Gswin.FullName;
                    // process - Arguments
                    p.StartInfo.Arguments = Arguments;
                    // process - silent
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.UseShellExecute = false;
                    // process - start
                    p.Start();
                    // process - wait for completion
                    p.WaitForExit();

                    foreach (FileInfo x in Delete)
                    {
                        x.Delete();
                    }
                }
                else
                {
                    List<Process> processes = new List<Process>();
                    foreach (FileInfo x in this.spooled)
                    {
                        string filename = Path.GetFileNameWithoutExtension(x.FullName);

                        string pdfFile = pdfOut + "\\" + filename + ".pdf";

                        result.Add(new FileInfo(pdfFile));

                        string Arguments = GsWin32ArgumentsPDF + "\"" + pdfFile + "\" \"" + x.FullName + "\"";

                        // start new proces
                        Process p = new Process();
                        // process - Executable file
                        p.StartInfo.FileName = Gswin.FullName;
                        // process - Arguments
                        p.StartInfo.Arguments = Arguments;
                        // process - silent
                        p.StartInfo.CreateNoWindow = true;
                        p.StartInfo.UseShellExecute = false;
                        // process - start
                        p.Start();

                        processes.Add(p);
                    }

                    while (!processes.Any(x => x.HasExited))
                    {
                    }

                    foreach (FileInfo x in this.spooled)
                    {
                        x.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
            }

            spooled = new List<FileInfo>();
            return result;
        }

        public void JoinPdfFile(List<FileInfo> pdfs)
        {
            try
            {
                List<FileInfo> Delete = new List<FileInfo>();
                FileInfo first = pdfs.First();
                string Arguments = GsWin32ArgumentsJoinPDF + "\"" + first.Directory.FullName + "\\" + first.Directory.Name + ".pdf\" ";

                if (!Directory.Exists(MergePath))
                {
                    Directory.CreateDirectory(MergePath);
                }

                if (!Directory.Exists(MergePath))
                {
                    MergePath = Context.Configuration.GetProductPath("#");
                }
                // Copy All PS file to the merge Path
                foreach (FileInfo s in pdfs)
                {
                    FileInfo ps = s.CopyTo(MergePath + "\\" + s.Name, true);
                    Arguments += "\"" + ps.FullName + "\" ";
                    Delete.Add(ps);
                }

                // start new proces
                Process p = new Process();
                // process - Executable file
                p.StartInfo.FileName = Gswin.FullName;
                // process - Arguments
                p.StartInfo.Arguments = Arguments;
                // process - silent
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.UseShellExecute = false;
                // process - start
                p.Start();
                // process - wait for completion
                p.WaitForExit();

                foreach (FileInfo s in Delete)
                {
                    s.Delete();
                }
            }
            catch (Exception)
            {
            }
        }

        public void Spool(List<Figure> figures, DrawingSheet sheet, string fileName, string directory)
        {
            // NOTE: hicad prints PLO > PdfCreator converts to PS -> GsWin32 converts to PDF
            try
            {
                if (figures == null || figures.Count == 0)
                    return;

                this.OutputDirectory = directory;
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                // do we need leading zero's ? 01 instead of 1
                int frameFormat = figures.Count().ToString().Length;
                int frameIndex = 0;

                foreach (Figure figure in figures.OrderBy(f => f.BoundingRect.BottomLeft.Y).ThenBy(f => f.BoundingRect.BottomLeft.X))
                {
                    // get the name of the provided Frame Name
                    string frameName = figure.Name;
                    // Use Regex Expression to determine the Size and Portrait/Landscape mode
                    Match match = Regex.Match(frameName, "^DIN(.[0-9]+)", RegexOptions.IgnoreCase);

                    // path() + Filename
                    string FilePLO = Path.GetTempPath() + fileName;

                    // append DrawingSheet index? niet wenselijk door krepla
                    //if (Context.ActiveScene.DrawingSheets.Count() > 2)
                    //{
                    //    // do we need leading zero's ? 01 instead of 1
                    //    int sheetFormat = Context.ActiveScene.DrawingSheets.OrderBy(x => x.Index).Select(x => x.Index).Last().ToString().Length;
                    //    // Append _{index}
                    //    FilePLO += "_" + sheet.Index.ToString(new String('0', sheetFormat));
                    //}

                    // append Sheet nubmer index?
                    if (figures.Count() > 1)
                    {
                        frameIndex++;
                        FilePLO += "_" + frameIndex.ToString(new String('0', frameFormat));
                    }

                    FilePLO += ".PLO";

                    try
                    {
                        if (File.Exists(FilePLO))
                        {
                            File.Delete(FilePLO);
                        }
                    }
                    catch (Exception ex)
                    {
                    }

                    if (match.Success)
                    {
                        ISD.Math.Point2D BL = figure.BoundingRect.BottomLeft;
                        ISD.Math.Point2D TR = figure.BoundingRect.TopRight;

                        // Create new Plo settings, used for printing and setting the Section to be printed
                        PLOSettings PloSettings = new PLOSettings
                        {
                            ViewSection = figure.GetGeoFrame(false)
                        };
                        // Redraw Screen to be sure
                        //Context.EnforceRedraw();

                        // Create a PLO file with the provided settings
                        FileIO.Save(FilePLO, PloSettings);

                        int c = 0;
                        // Wait until HiCAD/PLO file is deleted skips if is already deleted
                        while (!File.Exists(HicadPloFile) && c <= 200)
                        {
                            // increment counter in order to prevent freeze
                            c++;
                            // wait 0.1 sec.
                            System.Threading.Thread.Sleep(10);
                        }

                        c = 0;
                        // Wait until HiCAD/PLO is created
                        while (File.Exists(HicadPloFile) && c <= 200)
                        {
                            c++;
                            try
                            {
                                // Delete me :D
                                File.Delete(HicadPloFile);
                            }
                            catch (Exception)
                            {
                                // Failed Deleting, wait 0.1 sec and try again!
                                System.Threading.Thread.Sleep(100);
                            }
                        }

                        c = 0;
                        // Wait until the file is deleted in order to print new sheet or what ever
                        while (File.Exists(HicadPloFile) && c <= 200)
                        {
                            c++;
                            System.Threading.Thread.Sleep(10);
                            if (c >= 100) return;
                        }

                        try
                        {
                            HicadPrinter.ReadConstructionColorTableFromSZA(Context.ActiveScene.Name);

                            // Select PaperName eg: A3, A4
                            HicadPrinter.SelPaperSizeIDFromName(match.Groups[1].Value);
                            // Print in Portrait or Landscape mode set to 2
                            HicadPrinter.lPageOrientation = figure.BoundingRect.Width > figure.BoundingRect.Height ? 2 : 1;

                            // Name in the title bar displayed in Chrome
                            HicadPrinter.strDocumentName = @"C:/" + fileName + ".pdf";
                            // output PostScript file
                            HicadPrinter.strOutputFileName = Path.ChangeExtension(FilePLO, ".PS");
                            // PLO file to be printed / used  as source
                            HicadPrinter.PrintFile(FilePLO);
                            spooled.Add(new FileInfo(HicadPrinter.strOutputFileName));

                            File.Delete(FilePLO);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}