﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Smart_Printer
{
    public class Program
    {
        private static Window windowView = null;

        public static void Run()
        {
            // check datum
            DateTime CurrentDate = DateTime.Today;
            DateTime ExpirationDate;
            DateTime.TryParse("12 / 1 / 2020", out ExpirationDate); // maand / dag / jaar
            TimeSpan span = ExpirationDate.Date.Subtract(CurrentDate.Date);
            int daysToExpiration = Convert.ToInt32(span.TotalDays);

            if (daysToExpiration <= 15 && daysToExpiration > 0)
            {
                MessageBox.Show(daysToExpiration.ToString() + " Days Left Till Your Testing Period Ends", "Testing Period About to Ends", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else if (daysToExpiration <= 0)
            {
                MessageBox.Show("Your Testing Period has Ended" + Environment.NewLine + "Please Contact Vroba" + Environment.NewLine +
                     "+31 (0)183 - 601 451", "Testing Period Ended", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            //check domein

            string CurrentDomein = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
            string VrobaDomein = "vroba";
            string KlantDomein = "krepla";

            if (CurrentDomein.Contains(VrobaDomein) || CurrentDomein.Contains(KlantDomein))
            {
            }
            else
            {
                MessageBox.Show("Domain Doesn't Match" + Environment.NewLine +
                    "Please Contact Vroba" + Environment.NewLine +
                    "https://www.vroba.nl/", "Wrong Domain!!");
                return;
            }

            if (windowView == null || windowView.IsLoaded == false)
            {
                Windows.MainWindow windowContent = new Windows.MainWindow();
                string assVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

                List<string> assVersions = assVersion.Split('.').ToList();
                assVersions.RemoveAt(3);

                assVersion = String.Join(".", assVersions);

                windowView = windowContent;
                windowView.Topmost = true;
                windowView.Title = "Smart Printer | Test V" + assVersion;
                windowView.Width = windowContent.Width;
                windowView.Height = windowContent.Height;
                windowView.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                //windowView.WindowStyle = WindowStyle.ToolWindow;
                windowView.WindowState = WindowState.Normal;
                windowView.Show();
            }
            else
            {
                windowView.Show();
                windowView.WindowState = WindowState.Normal;
            }
        }

        public void EventBeforeMacEnd()
        {
        }

        public void EventBeforeMacStart()
        {
        }
    }
}