﻿//<debug />
using ISD.CAD.Contexts;
using ISD.CAD.Data;
using System.Collections.Generic;
using System.Linq;

internal class Script : ISD.Scripting.ScriptBase
{
    public static UnconstrainedContext Context
    {
        get { return BaseContext as UnconstrainedContext; }
    }

    [ISD.Scripting.Context(typeof(UnconstrainedContext))]
    public static void Main()
    {
        Dictionary<int, Figure> dicBorders = new Dictionary<int, Figure>();
        List<Figure> borders = new List<Figure>();
        foreach (Figure f in Context.ActiveScene.ActiveDrawingSheet.Figures)
        {
            if (f.Name.Contains("DIN"))
            {
                borders.Add(f);
            }
        }

        borders.OrderByDescending(x => x.BoundingRect.BottomRight.Y).ThenBy(x => x.BoundingRect.BottomRight.X);
        int i = 1;

        foreach (Figure f in borders)
        {
            dicBorders.Add(i, f);
            i++;
        }

        Dictionary<double, double> xycoordinates = new Dictionary<double, double>();

        foreach (var v in dicBorders)
        {
            System.Console.WriteLine(v.Key.ToString());
            System.Console.WriteLine(v.Value.BoundingRect.BottomRight.X);
            System.Console.WriteLine(v.Value.BoundingRect.BottomRight.Y);
        }
    }
}