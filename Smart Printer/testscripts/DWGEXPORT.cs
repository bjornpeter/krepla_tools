﻿//<debug />
//<assembly>API/ISD.CAD.IO.dll</assembly>
using ISD.CAD.Contexts;
using ISD.CAD.IO;
using System.Collections.Generic;
using System.Linq;
using System;

internal class Script : ISD.Scripting.ScriptBase
{
    public static UnconstrainedContext Context
    {
        get { return BaseContext as UnconstrainedContext; }
    }

    [ISD.Scripting.Context(typeof(UnconstrainedContext))]
    public static void Main()
    {
        Context.ActiveScene.ActiveDrawingSheet.Activate();
        string FileDWG = @"C:\USERS\BJORN\DESKTOP\KREPLA TESTING\TEST PROJECT\08 TEKENING\test dwg\test-dwg.DWG";
        DXFDWGSettings dwg = new DXFDWGSettings()
        {
            DrawingSheet = Context.ActiveScene.ActiveDrawingSheet
        };
        try
        {
            ISD.CAD.IO.FileIO.Save(FileDWG, dwg);
        }
        catch (Exception ex)
        {
        }
    }
}